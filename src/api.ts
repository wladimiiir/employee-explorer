import axios from "axios";
import {uniq, flatten} from "lodash";

const BASE_URL = "http://api.additivasia.io/api/v1";

const KEY_DIRECT_SUBORDINATES = "direct-subordinates";

const get = (path: string) => axios.get(`${BASE_URL}${path}`);

export const getAllSubordinates = async (employee: string): Promise<string[]> => uniq(await getSubordinates([employee]));

const getSubordinates = async (employees: string[]) => {
    const fetchSubordinates = async (employee: string) => {
        const {data} = await get(`/assignment/employees/${employee}`);
        return (data[1] && data[1][KEY_DIRECT_SUBORDINATES]) || [];
    };

    if (employees.length === 0) {
        return [];
    }

    const subordinates = uniq(
        flatten(
            await Promise.all(employees.map(fetchSubordinates))
        )
    );
    return subordinates.concat(await getSubordinates(subordinates));
};
