import React, {createContext, useMemo} from "react";
import {withRouter} from "react-router-dom";

export type NavigationType = {
    navigateTo(path: string);
    showExplorer();
    showOverview(employee: string);
};

const NavigationContext = createContext<NavigationType>(null as unknown as NavigationType);
export default NavigationContext;

type Props = {
    history: { push: Function };
    children: React.ReactNode;
}

export const NavigationProvider = withRouter(
    ({history, children}: Props) => {
        const navigation = useMemo(() => ({
            navigateTo: path => history.push(path),
            showExplorer: () => history.push("/explorer"),
            showOverview: (employee: string) => history.push(`/overview/${employee}`)
        }), [history]);

        return (
            <NavigationContext.Provider value={navigation}>
                {children}
            </NavigationContext.Provider>
        );
    }
);
