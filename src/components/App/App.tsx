import React from "react";
import {Route, Switch, Redirect} from "react-router-dom";

import {Explorer, Overview} from "components";
import {NavigationProvider} from "contexts";

import "./app.scss";

const App = () => {
    return (
        <NavigationProvider>
            <div className="app">
                <Switch>
                    <Route path="/explorer" component={Explorer}/>
                    <Route path="/overview/:employee" render={({match}) => <Overview employee={match.params.employee}/>}/>
                    <Redirect to="/explorer"/>
                </Switch>
            </div>
        </NavigationProvider>
    );
};

export default App;
