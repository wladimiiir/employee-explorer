import React, {useContext} from "react";
import {Input} from "antd";

import {NavigationContext} from "contexts";

import "./explorer.scss";

const {Search} = Input;

const Explorer = () => {
    const {showOverview} = useContext(NavigationContext);

    const search = (employee: string) => showOverview(employee);

    return (
        <div className="explorer">
            <h1>Employee Explorer</h1>
            <Search
                className="search"
                placeholder="Enter employee name"
                enterButton="Find"
                size="large"
                onSearch={search}
                autoFocus
            />
        </div>
    );
};

export default Explorer;
