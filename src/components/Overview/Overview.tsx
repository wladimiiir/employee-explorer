import React, {useContext, useEffect, useState} from "react";
import {Button, Icon} from "antd";

import {getAllSubordinates} from "api";
import {NavigationContext} from "contexts";

import "./overview.scss";

type Props = {
    employee: string;
}

const Overview = ({employee}: Props) => {
    const {showExplorer} = useContext(NavigationContext);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);
    const [subordinates, setSubordinates] = useState<string[]>([]);

    const loadSubordinates = async (employee: string) => {
        setLoading(true);

        try {
            const subordinates = await getAllSubordinates(employee);
            setSubordinates(subordinates);
        } catch (error) {
            if (error.response && error.response.status === 404) {
                setError(`No employee with name ${employee} found.`);
            } else {
                setError(error.message);
            }
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        loadSubordinates(employee);
    }, [employee]);

    const toSubordinate = (subordinate: string) => (
        <div key={subordinate} className="subordinate">{subordinate}</div>
    );

    return (
        <div className="overview">
            <h1>Employee Overview</h1>
            {
                loading &&
                <div className="loading">
                    <Icon type="loading"/>
                    <div className="loadingText">Loading subordinates of <b>{employee}</b>...</div>
                </div>
            }
            {
                !loading && !error &&
                <>
                    <div className="employee">Subordinates of employee <b>{employee}</b>:</div>
                    <div className="subordinates">
                        {
                            subordinates.length === 0 &&
                            <div className="noSubordinates">No subordinates found</div>
                        }
                        {subordinates.map(toSubordinate)}
                    </div>
                </>
            }
            {
                !loading && error &&
                <div className="error">{error}</div>
            }
            <Button
                className="backButton"
                type="link"
                onClick={showExplorer}
                disabled={loading}
            >
                Back
            </Button>
        </div>
    );
};

export default Overview;
